﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour
{
    [SerializeField] List<AudioSource> AudioSources;

    #region Public Getters / Setters

    public enum Sound
    {
        Music = 0,
        FootStepPlayer = 1,
        FootStepClone = 2,
        Jump1 = 3,
        Jump2 = 4,
        Jump3 = 5,
        DoorOpening = 6,
        DoorClosing = 7,
        CloneRecord = 8,
        CloneRewind = 9,
        ButtonPressed = 10,
        BatAmbiant1 = 11,
        BatAmbiant2 = 12,
        BatAmbiant3 = 13,
        RockAmbiant1 = 14,
        RockAmbiant2 = 15,
        RockAmbiant3 = 16, 
        RockAmbiant4 = 17,
        CubePushed = 18,
        EndJump1 = 19,
        EndJump2 = 20,
        EndJump3 = 21,
        TotalSoundCount = 22
    }

    public static SoundSystem Instance()
    {
        return instance;
    }

    static public Sound SelectRandom(Sound[] sounds)
    {
        float range = 1.0f / sounds.Length;
        int selected = Mathf.FloorToInt(Random.Range(0.0f, 1.0f) / range);
        return sounds[selected];
    }

    public void Play(Sound sound)
    {
        AudioSources[(int)sound].Play();
    }

    public void Pause(Sound sound)
    {
        AudioSources[(int)sound].Pause();
    }

    public void Stop(Sound sound)
    {
        AudioSources[(int)sound].Stop();
    }

    #endregion

    #region Editor callbacks

    void Awake()
    {
        instance = this;
        if (AudioSources.Count != (int) Sound.TotalSoundCount)
            Debug.LogError("Number of AudioSource component is different of the number of enum Sound");
    }

    void Start()
    {
        StartCoroutine(AmbiantSoundRoutine());
    }

    #endregion

    #region Private methods

    IEnumerator AmbiantSoundRoutine()
    {
        Sound[] ambiantSounds = new[] { Sound.BatAmbiant1, Sound.BatAmbiant2, Sound.BatAmbiant3, Sound.RockAmbiant1, Sound.RockAmbiant2, Sound.RockAmbiant3, Sound.RockAmbiant4};
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(20, 30));
            Play(SelectRandom(ambiantSounds));
        }
    }

    #endregion

    #region Variables

    static SoundSystem instance;

    #endregion
}
