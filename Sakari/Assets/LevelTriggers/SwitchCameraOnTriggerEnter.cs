﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SwitchCameraOnTriggerEnter : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera cinCam;
    [SerializeField] float SecondsToWait = 4;

    ICinemachineCamera mainCam;
    bool hasOccurred = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !hasOccurred)
        {
            mainCam = Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera;
            StartCoroutine(SwitchCamera());
            hasOccurred = true;
        }
    }

    IEnumerator SwitchCamera()
    {
        cinCam.Priority = 1;
        mainCam.Priority = 0;

        yield return new WaitForSeconds(SecondsToWait);

        cinCam.Priority = 0;
        mainCam.Priority = 1;
    }
}
