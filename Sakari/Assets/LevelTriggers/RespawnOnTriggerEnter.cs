﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnOnTriggerEnter : MonoBehaviour
{
    [SerializeField] GameObject respawnPosition;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.transform.position = respawnPosition.transform.position;
            var rb = other.gameObject.GetComponent<Rigidbody>();
            if (rb)
                rb.velocity = Vector3.zero;
        }
    }
}
