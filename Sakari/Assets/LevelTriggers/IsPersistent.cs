﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsPersistent : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
