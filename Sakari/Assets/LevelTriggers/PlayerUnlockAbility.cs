﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnlockAbility : MonoBehaviour
{
    [SerializeField] PlayerAbilities abilityToUnlock;

    public void UnlockAbility()
    {
        GameState.Instance().AddPlayerAbility(abilityToUnlock);
    }
}
