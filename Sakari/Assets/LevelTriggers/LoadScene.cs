﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    [SerializeField] string levelToLoad;
    [SerializeField] string levelToUnload;
    static List<string> loadedLevels = new List<string>();
    static bool firstInstance = true;

    void Awake()
    {
        if (firstInstance)
        {
            loadedLevels.Add(SceneManager.GetActiveScene().name);
            firstInstance = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (levelToLoad != "" && !loadedLevels.Contains(levelToLoad))
            {
                loadedLevels.Add(levelToLoad);
                StartCoroutine(LoadSceneAsync(levelToLoad));
            }
            if (levelToUnload != "" && loadedLevels.Contains(levelToUnload))
            {
                loadedLevels.Remove(levelToUnload);
                StartCoroutine(UnLoadSceneAsync(levelToUnload));
            }
        }
    }

    IEnumerator LoadSceneAsync(string name)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        asyncLoad.priority = 1;
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    IEnumerator UnLoadSceneAsync(string name)
    {
        AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(name);
        asyncLoad.priority = 0;
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
