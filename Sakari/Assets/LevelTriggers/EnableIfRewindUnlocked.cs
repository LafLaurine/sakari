﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableIfRewindUnlocked : MonoBehaviour, IAbilityChangedListener
{
    void Start()
    {
        GameState.Instance().AddAbilityChangedListener(this);
        if (!GameState.Instance().PlayerHasAbility(PlayerAbilities.Rewind))
            gameObject.SetActive(false);
    }

    public void OnAbilityChanged(PlayerAbilities ability, bool enabled)
    {
        if (ability == PlayerAbilities.Rewind)
        {
            if (enabled)
                gameObject.SetActive(true);
            else
                gameObject.SetActive(false);
        }
    }
}
