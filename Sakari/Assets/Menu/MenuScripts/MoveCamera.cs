﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour
{
    Camera m_camera;
    Vector3 m_initPostion;

    private void Start()
    {
        m_camera = GetComponent<Camera>();
        m_initPostion = m_camera.transform.position;
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * 0.2f);
        if(m_camera.transform.position.z > -374.56)
        {
            m_camera.transform.position = new Vector3(m_initPostion.x, m_initPostion.y, m_initPostion.z);
        }
    }
}