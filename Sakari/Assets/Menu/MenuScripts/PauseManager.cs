﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    private PauseAction action;
    private bool paused = false;

    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject controlsMenu;

    private void Awake()
    {
        action = new PauseAction();
    }

    private void OnEnable()
    {
        action.Enable();
    }

    private void OnDisable()
    {
        action.Disable();
    }

    private void Start()
    {
        action.Pause.PauseGame.performed += _ => DeterminePause();
    }

    private void DeterminePause()
    {
        if (paused)
            ResumeGame();
        else
            PauseGame();
    }

    public void PauseGame()
    {
        GameState.Instance().SetGamePause(true);
        Cursor.lockState = CursorLockMode.Confined;
        paused = true;
        controlsMenu.SetActive(false);
        pauseMenu.SetActive(true);
        ClickSound();
    }

    public void ResumeGame()
    {
        GameState.Instance().SetGamePause(false);
        Cursor.lockState = CursorLockMode.Locked;
        paused = false;
        pauseMenu.SetActive(false);
        controlsMenu.SetActive(false);
    }

    public void GoBackToControlsMenu()
    {
        controlsMenu.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void GoBackToMainMenu()
    {
        pauseMenu.SetActive(true);
        controlsMenu.SetActive(false);
    }

    private void ClickSound()
    {
        GetComponent<AudioSource>().Play();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
