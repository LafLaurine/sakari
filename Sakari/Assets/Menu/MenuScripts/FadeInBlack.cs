﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInBlack : MonoBehaviour
{
    [SerializeField] GameObject fadeOutSquare;
    [SerializeField] GameObject HUD;
    [SerializeField] GameObject endingScreen;

    void Start()
    {
        endingScreen.SetActive(false);
    }

    public void StartFadeIn()
    {
        StartCoroutine(FadeInBlackSquare());
    }

    IEnumerator FadeInBlackSquare(float fadeSpeed = 0.0555f)
    {
        Color objectColor = fadeOutSquare.GetComponent<Image>().color;
        for (float i = objectColor.a; i <= 1.0f; i+=fadeSpeed)
        {
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, i);
            fadeOutSquare.GetComponent<Image>().color = objectColor;
            yield return new WaitForSeconds(0.2f);
        }
        HUD.SetActive(false);
        endingScreen.SetActive(true);
        yield return null;
    }
}
