﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerAbilities
{
    Rewind
};

public enum PlayerInputState
{
    UI, // TODO check https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/UISupport.html
    ControlCharacter
};

public interface IAbilityChangedListener
{
    void OnAbilityChanged(PlayerAbilities ability, bool enabled);
}

public interface IPlayerInputStateListener
{
    void OnPlayerInputChanged(PlayerInputState state);
}

public class GameState : MonoBehaviour
{
    #region Editor References

    [SerializeField] List<PlayerAbilities> startAbilities;
    [SerializeField] PlayerInputState inputState = PlayerInputState.ControlCharacter;

    #endregion

    #region Public Getters / Setters

    public static GameState Instance()
    {
        return instance;
    }

    public void SetGamePause(bool enabled)
    {
        if (enabled)
        {
            Time.timeScale = 0;
            AudioListener.pause = true;
            SetPlayerInputState(PlayerInputState.UI);
        }
        else
        {
            Time.timeScale = 1;
            AudioListener.pause = false;
            SetPlayerInputState(PlayerInputState.ControlCharacter);
        }
    }

    public PlayerInputState GetPlayerInputState() { return inputState; }

    public void SetPlayerInputState(PlayerInputState state)
    {
        inputState = state;
        foreach (var listener in inputChangedListeners)
            listener.OnPlayerInputChanged(state);
    }

    public void AddPlayerInputStateChangedListener(IPlayerInputStateListener listener)
    {
        if (!inputChangedListeners.Contains(listener))
            inputChangedListeners.Add(listener);
    }

    public void RemovePlayerInputStateChangedListener(IPlayerInputStateListener listener)
    {
        if (inputChangedListeners.Contains(listener))
            inputChangedListeners.Remove(listener);
    }

    public bool PlayerHasAbility(PlayerAbilities ability) { return startAbilities.Contains(ability); }

    public void AddPlayerAbility(PlayerAbilities ability)
    {
        if (!startAbilities.Contains(ability))
        {
            startAbilities.Add(ability);
            foreach (var listener in abilityChangedListeners)
                listener.OnAbilityChanged(ability, true);
        }
    }

    public void AddAbilityChangedListener(IAbilityChangedListener listener)
    {
        if (!abilityChangedListeners.Contains(listener))
            abilityChangedListeners.Add(listener);
    }

    public void RemoveAbilityChangedListener(IAbilityChangedListener listener)
    {
        if (abilityChangedListeners.Contains(listener))
            abilityChangedListeners.Remove(listener);
    }

    #endregion

    #region

    private void Awake()
    {
        instance = this;
    }

    #endregion

    #region Variables

    static GameState instance;
    List<IAbilityChangedListener> abilityChangedListeners = new List<IAbilityChangedListener>();
    List<IPlayerInputStateListener> inputChangedListeners = new List<IPlayerInputStateListener>();

    #endregion
}
