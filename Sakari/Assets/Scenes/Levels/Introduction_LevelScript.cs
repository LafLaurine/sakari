﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class Introduction_LevelScript : MonoBehaviour
{
    [SerializeField] AudioClip walkInIce;
    [SerializeField] AudioClip iceCracks;

    AudioSource source;

    public void PlayIceCracksAudio()
    {
        source.clip = iceCracks;
        source.Play();
        StartCoroutine(LoadLevelAfter(3));
    }

    IEnumerator LoadLevelAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        SceneManager.LoadScene("InitiatoryPath");
    }

    void Start()
    {
        source = GetComponent<AudioSource>();

        // Play start dialog
        source.clip = walkInIce;
        source.volume = source.volume / 4.0f;
        source.Play();
    }
}
