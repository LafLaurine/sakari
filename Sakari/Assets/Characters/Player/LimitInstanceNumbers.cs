﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitInstanceNumbers : MonoBehaviour
{
    static uint instanceCount = 0;
    void Awake()
    {
        instanceCount++;
        if (instanceCount > 1)
        {
            Debug.LogError("[LimitInstanceNumbers] You created 2 instance of this object, please fix this. Now it will be destroyed");
            Destroy(this.gameObject);
        }  
    }
}
