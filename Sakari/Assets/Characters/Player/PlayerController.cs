using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public enum CharacterState
{
    MoveFree,
    Carrying,
    ButtonPress
}

public class PlayerController : MonoBehaviour
{
    #region Editor References

    [SerializeField] bool isClone = false;

    #endregion

    #region Public Getters / Setters
    public CharacterState charState { get; private set; } = CharacterState.MoveFree;
    public Vector3 Movement { get; private set; }
    public bool WantsToJump { get; private set; }
    public bool WantsToInteract { get; private set; }

    public void AddAvailableInteractible(IInteractible o)
    {
        if (!interactibleInRange.Contains(o))
            interactibleInRange.Add(o);
    }

    public void RemoveInteractible(IInteractible o)
    {
        if (interactibleInRange.Contains(o))
            interactibleInRange.Remove(o);
    }

    public void ClearAllInteractibles()
    {
        interactibleInRange.Clear();
    }

    #endregion

    #region Public Input Callbacks

    public void OnMove(Vector2 movement)
    {
        Movement = new Vector3(movement.x, 0, movement.y);
    }

    public void OnCloneMove(Vector2 movement, float cameraY)
    {
        this.cameraY = cameraY;
        Movement = new Vector3(movement.x, 0, movement.y);
    }

    public void OnJump(bool pressed)
    {
        WantsToJump = pressed;
        if (pressed)
            animator.SetBool("isGrounded", false);
    }

    public void OnInteract(bool pressed)
    {
        WantsToInteract = pressed;
        if (!WantsToInteract || interactibleInRange.Count == 0)
            return;

        switch (interactibleInRange[0].GetInteractibleType())
        {
            case InteractibleType.Pressable:
            case InteractibleType.Popupable:
                interactibleInRange[0].EnableMechanism();
                break;
        }
    }

    #endregion

    #region Variables

    // Private References
    Rigidbody rb;
    Animator animator;
    Vector3 colExtents;
    Timeline timeline;

    // Personalisation
    readonly float jump = 5f;

    // State
    bool isRunning = true;
    float cameraY;
    List<IInteractible> interactibleInRange = new List<IInteractible>();
    float Run => isRunning ? 2f : 1f;
    bool isWalking = false;
    bool isJumping = false;

    #endregion

    #region Unity Callbacks

    void Awake()
    {

        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        colExtents = GetComponent<Collider>().bounds.extents;
        if (!isClone)
            timeline = GetComponent<Timeline>();

        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate()
    {
        // Update animator
        animator.SetFloat("ForwardSpeed", Movement.magnitude);
        animator.SetBool("isGrounded", IsGrounded());

        if (IsGrounded() && isJumping)
        {
            SoundSystem.Instance().Play(SoundSystem.SelectRandom(new[] { SoundSystem.Sound.EndJump1, SoundSystem.Sound.EndJump2, SoundSystem.Sound.EndJump3 }));
            isJumping = false;
        }

        // Character Jump
        if (IsGrounded() && WantsToJump) {
            rb.velocity = new Vector3(rb.velocity.x, jump, rb.velocity.z);
            SoundSystem.Instance().Play(SoundSystem.SelectRandom(new[] { SoundSystem.Sound.Jump1, SoundSystem.Sound.Jump2, SoundSystem.Sound.Jump3 }));
            isJumping = true;
        }

        MoveRigidbody();

        // TODO use callback from the animator instead
        // FootStep Sound
        if (Movement.magnitude > 0.1f && IsGrounded() && !isWalking)
        {
            SoundSystem.Instance().Play(isClone ? SoundSystem.Sound.FootStepClone : SoundSystem.Sound.FootStepPlayer);
            isWalking = true;
        }
        else if (Movement.magnitude < 0.1f && isWalking || !IsGrounded())
        {
            SoundSystem.Instance().Stop(isClone ? SoundSystem.Sound.FootStepClone : SoundSystem.Sound.FootStepPlayer);
            isWalking = false;
        }
    }

    #endregion

    #region Methods

    // FIXME not great
    // raycast between the player and the ground to see if he touchs it
    bool IsGrounded()
    {
        Ray ray = new Ray(this.transform.position + Vector3.up * 2 * colExtents.x, Vector3.down);
        return Physics.SphereCast(ray, colExtents.x, colExtents.x + 0.2f);
    }

    void MoveRigidbody()
    {
        // Prevent unwanted rotation when no input
        if (Movement.magnitude < 0.1)
            return;
        
        // Rotation based on camera direction. Clone already have these informations
        if (!isClone)
        {
            // Check if rewinding
            if (timeline.timeScale <= 0)
                return;
        }

        float angle = isClone ? cameraY : Camera.main.transform.rotation.eulerAngles.y;
        Vector3 forward = Quaternion.Euler(0f, angle, 0f) * Vector3.forward;
        forward.y = 0f;
        forward.Normalize();
        Quaternion targetRotation;

        // If the local movement direction is the opposite of forward then the target rotation should be towards the camera.
        if (Mathf.Approximately(Vector3.Dot(Movement, Vector3.forward), -1.0f))
            targetRotation = Quaternion.LookRotation(-forward);
        else
        {
            // Otherwise the rotation should be the offset of the input from the camera's forward.
            Quaternion cameraToInputOffset = Quaternion.FromToRotation(Vector3.forward, Movement);
            targetRotation = Quaternion.LookRotation(cameraToInputOffset * forward);
        }

        // Move in the direction the camera is facing
        rb.MoveRotation(targetRotation);
        rb.MovePosition(rb.position + rb.transform.forward * Movement.magnitude * Run * 0.05f); 
    }




    #endregion
}
