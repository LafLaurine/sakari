﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Chronos;
using Cinemachine;

enum RewindState
{
    Record,
    Rewind
}

public class RewindManager : MonoBehaviour, IPlayerInputStateListener, IAbilityChangedListener
{
    #region Editor references

    [SerializeField] GameObject clone;
    [SerializeField] GameObject player;
    [SerializeField] Animator rewindVFXAnimator;
    [SerializeField] Slider slider;

    #endregion

    #region Public methods

    public void OnPlayerInputChanged(PlayerInputState state)
    {
        if (state == PlayerInputState.UI)
            inputControls.Disable();
        else
            inputControls.Enable();
    }

    public void OnAbilityChanged(PlayerAbilities ability, bool enabled)
    {
        if (ability == PlayerAbilities.Rewind)
        {
            if (enabled)
                inputControls.PlayerControls.Rewind.Enable();
            else
                inputControls.PlayerControls.Rewind.Disable();
        }
    }

    #endregion

    #region Editor callbacks

    void Start()
    {
        clock = Timekeeper.instance.Clock("Root");

        // Create hidden clone
        clone = Instantiate(clone, player.transform.position, player.transform.rotation);
        Physics.IgnoreCollision(clone.GetComponent<Collider>(), player.GetComponent<Collider>());
        clone.SetActive(false);

        // Get controllers
        cloneController = clone.GetComponent<PlayerController>();
        playerController = player.GetComponent<PlayerController>();

        // Init vars
        playerInputBuffer = new List<InputState>();
        cloneInputBuffer = new Queue<InputState>();
        // Max timeline in seconds * 25 frames per seconds
        maxRewindFrames = (uint) playerController.GetComponent<Timeline>().recordingDuration * 25;

        // Setup Input callbacks for player
        inputControls = new Controls();
        inputControls.Enable();
        inputControls.PlayerControls.Move.performed += ctx => playerController.OnMove(ctx.ReadValue<Vector2>());
        inputControls.PlayerControls.Move.canceled += ctx => playerController.OnMove(new Vector2(0, 0));
        inputControls.PlayerControls.Jump.performed += ctx => playerController.OnJump(true);
        inputControls.PlayerControls.Jump.canceled += ctx => playerController.OnJump(false);
        inputControls.PlayerControls.Interact.performed += ctx => playerController.OnInteract(true);
        inputControls.PlayerControls.Interact.canceled += ctx => playerController.OnInteract(false);

        // Setup Input callbacks for rewind manager
        inputControls.PlayerControls.Rewind.performed += ctx => OnRewindPressed(true);
        inputControls.PlayerControls.Rewind.canceled += ctx => OnRewindPressed(false);

        // Disable rewind if not enabled
        if (!GameState.Instance().PlayerHasAbility(PlayerAbilities.Rewind))
            inputControls.PlayerControls.Rewind.Disable();

        // Bind event
        GameState.Instance().AddPlayerInputStateChangedListener(this);
        GameState.Instance().AddAbilityChangedListener(this);

        // Setup camera
        var cam = (CinemachineFreeLook) Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera;
        if (cam)
        {
            cam.m_YAxis.m_InvertInput = PlayerPrefs.GetInt("masterInvertY") == 1 ? true : false;
        }
    }

    void FixedUpdate()
    {
        UpdateSliderValue();
        switch (rewindState)
        {
            case RewindState.Record:
                playerInputBuffer.Add(new InputState(
                    playerController.Movement,
                    Camera.main.transform.rotation.eulerAngles.y,
                    playerController.WantsToInteract,
                    playerController.WantsToJump)
                );

                if (playerInputBuffer.Count > maxRewindFrames)
                    playerInputBuffer.RemoveAt(0);

                if (remainingFramesToReplay > 0)
                    remainingFramesToReplay--;

                if (remainingFramesToReplay == 0)
                    StopClone();
                
                if (clone.activeSelf)
                    SetCloneInputs();
                break;

            case RewindState.Rewind:
                framesToReplay++;
                if (framesToReplay >= maxRewindFrames || framesToReplay >= playerInputBuffer.Count)
                    OnRewindPressed(false); // Launch clone now
                break;
        }
    }

    #endregion

    #region Private methods

    void OnRewindPressed(bool pressed)
    {
        if (pressed)
        {
            StopClone();
            rewindState = RewindState.Rewind;
            clock.localTimeScale = -1;
            rewindVFXAnimator.SetBool("On", true);
            SoundSystem.Instance().Play(SoundSystem.Sound.CloneRewind);
        }
        else if (!clone.activeSelf)
        {
            StartClone();
            rewindState = RewindState.Record;
            clock.localTimeScale = 1;
            rewindVFXAnimator.SetBool("On", false);
            SoundSystem.Instance().Stop(SoundSystem.Sound.CloneRewind);
            SoundSystem.Instance().Play(SoundSystem.Sound.CloneRecord);
        }
    }

    void StartClone()
    {
        // Fill clone buffer array
        int startIndex = playerInputBuffer.Count - framesToReplay; 
        playerInputBuffer.RemoveRange(0, startIndex);
        cloneInputBuffer = new Queue<InputState>(playerInputBuffer);

        // Reset values
        remainingFramesToReplay = framesToReplay;
        framesToReplay = 0;
        // Needed because chronos erease its data as well after rewind
        playerInputBuffer.Clear();

        // Start clone in world
        clone.transform.position = player.transform.position;
        clone.transform.rotation = player.transform.rotation;
        clone.SetActive(true);
    }

    void StopClone()
    {
        clone.SetActive(false);
        cloneController.ClearAllInteractibles();
        SoundSystem.Instance().Stop(SoundSystem.Sound.FootStepClone);
    }

    void SetCloneInputs()
    {
        var input = cloneInputBuffer.Dequeue();
        cloneController.OnJump(input.Jump);
        cloneController.OnInteract(input.Interact);
        cloneController.OnCloneMove(input.Movement, input.CameraY);
    }

    void UpdateSliderValue()
    {
        slider.value = 1f - ((maxRewindFrames - (playerInputBuffer.Count - framesToReplay)) / (float) maxRewindFrames);
    }

    #endregion

    #region Private variables

    // Components ref
    Clock clock;
    PlayerController cloneController;
    PlayerController playerController;

    // Private vars
    Controls inputControls;
    RewindState rewindState = RewindState.Record;
    Queue<InputState> cloneInputBuffer;
    List<InputState> playerInputBuffer;
    uint maxRewindFrames = 0;
    int framesToReplay = 0;
    int remainingFramesToReplay = 0;

    #endregion

    #region Private classes

    class InputState
    {
        public InputState(Vector3 Movement, float CameraY, bool Interact, bool Jump)
        {
            this.Movement = new Vector2(Movement.x, Movement.z); // y is always 0
            this.CameraY = CameraY;
            this.Interact = Interact;
            this.Jump = Jump;
        }

        public Vector2 Movement { get; private set; }
        public float CameraY { get; private set; }
        public bool Interact { get; private set; }
        public bool Jump { get; private set; }
    };

    #endregion
}
