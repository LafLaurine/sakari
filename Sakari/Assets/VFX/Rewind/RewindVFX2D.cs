﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindVFX2D : MonoBehaviour
{
    public float effectStrength = 0;
    public Material rewindMaterial;
    private UnityEngine.Rendering.HighDefinition.CustomPassVolume customPass;

    void Start()
    {
        customPass = GetComponent<UnityEngine.Rendering.HighDefinition.CustomPassVolume>();
    }

    void Update()
    {
        if (effectStrength < 0.000001) {
            customPass.enabled = false;
        }
        else {
            customPass.enabled = true;
            rewindMaterial.SetFloat("_EffectStrength", effectStrength);
        }
    }
}
