﻿Shader "FullScreen/RewindVFX2D"
{
    Properties
    {
        _EffectStrength ("Effect Strength", float) = 0
    }

    HLSLINCLUDE

    #pragma vertex Vert

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/RenderPass/CustomPass/CustomPassCommon.hlsl"

    uniform float _EffectStrength;

    float distBand(float R1, float R2, float r) {
                return (R2-R1)*0.5 - abs(r - (R1+R2)*0.5);
            }

            float crest(float a, float r) {
                return 5. * distBand(a*0.5, a, r);
            }

            float2 rotate(float a, float2 uv) {
                float c = cos(a);
                float s = sin(a);
                return float2(
                    c * uv.x - s * uv.y,
                    s * uv.x + c * uv.y
                );
            }

            float mod(float x, float y) {
                return x - y * floor(x/y);
            }

            float4 mod(float4 x, float y) {
                return x - y * floor(x/y);
            }

            //	Classic Perlin 2D Noise 
            //	by Stefan Gustavson
            // Thanks to https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83
            float2 fade(float2 t) {return t*t*t*(t*(t*6.0-15.0)+10.0);}

            float4 permute(float4 x){return mod(((x*34.0)+1.0)*x, 289.0);}

            float cnoise(float2 P){
            float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
            float4 Pf = frac(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
            Pi = mod(Pi, 289.0); // To avoid truncation effects in permutation
            float4 ix = Pi.xzxz;
            float4 iy = Pi.yyww;
            float4 fx = Pf.xzxz;
            float4 fy = Pf.yyww;
            float4 i = permute(permute(ix) + iy);
            float4 gx = 2.0 * frac(i * 0.0243902439) - 1.0; // 1/41 = 0.024...
            float4 gy = abs(gx) - 0.5;
            float4 tx = floor(gx + 0.5);
            gx = gx - tx;
            float2 g00 = float2(gx.x,gy.x);
            float2 g10 = float2(gx.y,gy.y);
            float2 g01 = float2(gx.z,gy.z);
            float2 g11 = float2(gx.w,gy.w);
            float4 norm = 1.79284291400159 - 0.85373472095314 * 
                float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11));
            g00 *= norm.x;
            g01 *= norm.y;
            g10 *= norm.z;
            g11 *= norm.w;
            float n00 = dot(g00, float2(fx.x, fy.x));
            float n10 = dot(g10, float2(fx.y, fy.y));
            float n01 = dot(g01, float2(fx.z, fy.z));
            float n11 = dot(g11, float2(fx.w, fy.w));
            float2 fade_xy = fade(Pf.xy);
            float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
            float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
            return 2.3 * n_xy;
            }

    float4 FullScreenPass(Varyings varyings) : SV_Target
    {
        float time = -_Time.y;
        float TAU = 6.2831853071;
        float N = 10.;
        float depth = LoadCameraDepth(varyings.positionCS.xy);
        PositionInputs posInput = GetPositionInput(varyings.positionCS.xy, _ScreenSize.zw, depth, UNITY_MATRIX_I_VP, UNITY_MATRIX_V);
        //
        float2 uv = posInput.positionNDC - float2(0.5, 0.5);
        uv *= 0.5;
        uv = rotate(time*0.6, uv);
        uv += 0.005*cnoise(uv*200.);
        float mask = 0.;
        // Main spirals
        float a = frac(0.5 - atan2(uv.x, uv.y) / TAU);
        float r = 1.5*length(uv)-0.23;
        for (float i = 0.; i < N; ++i) {
            mask += max(crest(frac(a + i/N), r), 0.);
        }
        mask *= _EffectStrength;
        // Output to screen
        float3 col = lerp(float3(1, 45, 64)*0.3, float3(166, 228, 255)*3., mask) / 255.;
        return float4(col, mask);
    }

    ENDHLSL

    SubShader
    {
        
        Pass
        {
            Name "Rewind VFX 2D"

            ZWrite Off
            ZTest Always
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            


            HLSLPROGRAM
                #pragma fragment FullScreenPass
            ENDHLSL
        }
    }
    Fallback Off
}
