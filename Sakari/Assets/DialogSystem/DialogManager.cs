﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public enum DialogCues
{
    ScreenShake,
    LargeFontSize,
    SmallFontSize
}

[System.Serializable]
public class Sentences
{
    public string narratorName;
    [TextArea(10, 10)]
    public string text;
    public DialogCues[] dialogCues;
    public UnityEvent dialogStartedCallback;
}

[System.Serializable]
public class Dialog
{
    public Sentences[] sentences;
    [Tooltip("in letters by seconds")] public float typingSpeed = 20.0f;
}

public enum DialogStart
{
    OnPlay,
    OnColliderEnter
}

public class DialogManager : MonoBehaviour
{
    #region Editor references

    public static DialogManager currentOpendialog = null;

    [SerializeField] bool playOnlyOnce = true;
    [SerializeField] bool stopPlayerMoves = false;
    [SerializeField] UnityEngine.UI.Text UITextNarrator;
    [SerializeField] UnityEngine.UI.Text UITextDialog;
    [SerializeField] UnityEngine.UI.Text UINextIndication;
    [SerializeField] Animator dialogHUD;
    [SerializeField] AudioSource dialogSound;
    [SerializeField] DialogStart dialogStart = DialogStart.OnPlay;
    [SerializeField] Dialog dialogToPlay;
    [SerializeField] UnityEvent dialogEndedCallback;

    private bool hasOccured = false;
    #endregion

    #region Public methods

    public void StartDialog()
    {
        if (playOnlyOnce)
        {
            if (hasOccured)
                return;
        }

        if (!dialogueStarted)
        {
            hasOccured = true;
            if (currentOpendialog != null)
            {
                currentOpendialog.StopAllCoroutines();
                currentOpendialog.dialogHUD.SetBool("interactive", false);
                currentOpendialog.dialogHUD.SetBool("open", false);
                currentOpendialog.dialogueStarted = false;
            }
            currentOpendialog = this;
            dialogueStarted = true;
            StartCoroutine(DisplayDialog(dialogToPlay));
        }
    }

    #endregion

    #region Editor callbacks

    void Awake()
    {
        inputControls = new Controls();
        inputControls.Enable();
        inputControls.PlayerControls.Interact.performed += ctx => displayNextSentence = true;

        var allGamepads = Gamepad.all;
        if (allGamepads.Count > 0)
            UINextIndication.text = "Button X";

        if (dialogStart == DialogStart.OnPlay)
            StartDialog();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" && dialogStart == DialogStart.OnColliderEnter)
        {
            StartDialog();
        }
    }

    #endregion

    #region Private methods

    IEnumerator TypeSentence(UnityEngine.UI.Text textObj, string sentence, float secByletter)
    {
        textObj.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogSound.Play();
            textObj.text += letter;
            yield return new WaitForSeconds(secByletter);
        }
        dialogSound.Stop();
    }

    IEnumerator DisplayDialog(Dialog dialog)
    {
        if (stopPlayerMoves)
            GameState.Instance().SetPlayerInputState(PlayerInputState.UI);

        UITextNarrator.text = dialog.sentences[0].narratorName;
        UITextDialog.text = "";
        dialogHUD.SetBool("open", true);
        yield return new WaitForSeconds(0.5f);

        foreach (var sentence in dialog.sentences)
        {
            sentence.dialogStartedCallback.Invoke();
            UITextNarrator.text = sentence.narratorName;
            yield return StartCoroutine(TypeSentence(UITextDialog, sentence.text, 1 / dialog.typingSpeed));
            displayNextSentence = false;

            dialogHUD.SetBool("interactive", true);
            yield return new WaitForSeconds(0.1f);

            while (!displayNextSentence) yield return null;

            dialogHUD.SetBool("interactive", false);
            yield return new WaitForSeconds(0.1f);
        }
        dialogEndedCallback.Invoke();
        dialogHUD.SetBool("open", false);

        dialogueStarted = false;

        if (stopPlayerMoves)
            GameState.Instance().SetPlayerInputState(PlayerInputState.ControlCharacter);
    }

    #endregion

    #region Private variables

    Controls inputControls;
    private bool dialogueStarted = false;
    private bool displayNextSentence = false;

    #endregion
}
