﻿
public enum InteractibleType
{
    Pushable,
    Pressable,
    Popupable,
    PickUpAble
}

public interface IInteractible
{
    InteractibleType GetInteractibleType();

    void EnableMechanism();

    void DisableMechanism();
}
