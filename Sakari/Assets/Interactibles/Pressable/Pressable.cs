﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pressable : MonoBehaviour, IInteractible
{
    [SerializeField] TMPro.TextMeshPro text;
    [SerializeField] DoorMechanism doorToActivate;
    [SerializeField] Material enabledMaterial;
    [SerializeField] float activationTimeInSeconds = 6;

    Material disabledMaterial = null;
    Renderer matRenderer = null;

    void Start()
    {
        if (text == null)
            Debug.LogError("Text is not assigned to the button !");

        matRenderer = GetComponent<Renderer>();
        disabledMaterial = matRenderer.material;
        text.gameObject.SetActive(false);
        gameObject.layer = 0; // default render layer
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Clone")
        {
            col.gameObject.GetComponent<PlayerController>().AddAvailableInteractible(this);
            text.gameObject.SetActive(true);
            gameObject.layer = 9; // Selection render layer
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Clone")
        {
            col.gameObject.GetComponent<PlayerController>().RemoveInteractible(this);
            text.gameObject.SetActive(false);
            gameObject.layer = 0; // default render layer
        }
    }


    #region IInteractible implementation

    public InteractibleType GetInteractibleType()
    {
        return InteractibleType.Pressable;
    }

    public void EnableMechanism()
    {
        doorToActivate.AddEnabledButton(this);
        StartCoroutine("Timer", activationTimeInSeconds);
        matRenderer.material = enabledMaterial;
        SoundSystem.Instance().Play(SoundSystem.Sound.ButtonPressed);
    }

    public void DisableMechanism()
    {
        doorToActivate.RemoveEnabledButton(this);
        matRenderer.material = disabledMaterial;
    }

    #endregion

    IEnumerator Timer(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        DisableMechanism();
    }
}
