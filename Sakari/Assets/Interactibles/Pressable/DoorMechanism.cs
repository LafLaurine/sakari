﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMechanism : MonoBehaviour
{
    [SerializeField] uint buttonsToActivate = 1;
    public bool lockable = true;
    List<IInteractible> buttonsActivated = new List<IInteractible>();

    public void AddEnabledButton(IInteractible button)
    {
        if (!buttonsActivated.Contains(button))
            buttonsActivated.Add(button);

        if (buttonsActivated.Count >= buttonsToActivate && !isOpen)
            OpenDoor();
    }

    public void RemoveEnabledButton(IInteractible button)
    {
        if (buttonsActivated.Contains(button))
            buttonsActivated.Remove(button);

        if (buttonsActivated.Count < buttonsToActivate && !lockable && isOpen)
            CloseDoor();
    }

    Renderer rend;
    float bufferTranslation = 0;
    bool isOpen = false;

    void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    void OpenDoor()
    {
        
        StopAllCoroutines();
        StartCoroutine(MoveDoor(-rend.bounds.size.y));
        SoundSystem.Instance().Play(SoundSystem.Sound.DoorOpening);
        isOpen = true;
        // TODO use particles
        // TODO callback on the buttons to lock them in enabled state
    }

    void CloseDoor()
    {
        
        StopAllCoroutines();
        StartCoroutine(MoveDoor(rend.bounds.size.y));
        SoundSystem.Instance().Play(SoundSystem.Sound.DoorClosing);
        isOpen = false;
    }

    IEnumerator MoveDoor(float YTranslation)
    {
        bufferTranslation += YTranslation;
        float range = bufferTranslation / 30.0f;
        while (Mathf.Abs(bufferTranslation) > Mathf.Abs(range))
        {
            bufferTranslation -= range;
            transform.Translate(Vector3.up * range);
            yield return new WaitForSeconds(0.03f);
        }
    }
}
