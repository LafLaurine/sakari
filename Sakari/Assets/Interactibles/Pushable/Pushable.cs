﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO
public class Pushable : MonoBehaviour
{



    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //SoundSystem.Instance().Play(SoundSystem.Sound.CubePushed);
            /*
           col.gameObject.GetComponent<PlayerController>().pushing = true;
           Vector3 direction = new Vector3(0, 0, transform.position.z);
           m_rb.MovePosition(transform.position + direction * Time.deltaTime);*/
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            //SoundSystem.Instance().Pause(SoundSystem.Sound.CubePushed);
        }
    }


    private Rigidbody rb;
    bool isMoving;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isMoving = false;
    }

    void FixedUpdate()
    {
        if (rb.velocity.magnitude > 0.2 && !isMoving)
        {
            isMoving = true;
            SoundSystem.Instance().Play(SoundSystem.Sound.CubePushed);
        } else if (rb.velocity.magnitude <= 0.2 && isMoving)
        {
            isMoving = false;
            SoundSystem.Instance().Pause(SoundSystem.Sound.CubePushed);
        }
    }
}
