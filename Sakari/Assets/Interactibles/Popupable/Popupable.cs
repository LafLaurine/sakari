﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popupable : MonoBehaviour, IInteractible
{
    [SerializeField] TMPro.TextMeshPro text;
    [SerializeField] Material enabledMaterial;
    [SerializeField] float activationTimeInSeconds = 6;
    [SerializeField] GameObject crate;
    [SerializeField] GameObject spawnArea;

    Material disabledMaterial = null;
    Renderer matRenderer = null;

    void Start()
    {
        if (text == null)
            Debug.LogError("Text is not assigned to the button !");

        matRenderer = GetComponent<Renderer>();
        disabledMaterial = matRenderer.material;
        text.gameObject.SetActive(false);
        gameObject.layer = 0;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Clone")
        {
            col.gameObject.GetComponent<PlayerController>().AddAvailableInteractible(this);
            text.gameObject.SetActive(true);
            gameObject.layer = 9; // Selection layer
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Clone")
        {
            col.gameObject.GetComponent<PlayerController>().RemoveInteractible(this);
            text.gameObject.SetActive(false);
            gameObject.layer = 0;
        }
    }

    #region IInteractible implementation

    public InteractibleType GetInteractibleType()
    {
        return InteractibleType.Popupable;
    }

    public void EnableMechanism()
    {
        crate.transform.position = spawnArea.transform.position;
        StartCoroutine("Timer", activationTimeInSeconds);
        matRenderer.material = enabledMaterial;
        // TODO feedback for player with sound
    }

    public void DisableMechanism()
    {
        matRenderer.material = disabledMaterial;
    }

    #endregion

    IEnumerator Timer(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        DisableMechanism();
    }

}
