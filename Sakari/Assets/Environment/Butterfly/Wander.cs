﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Wander : MonoBehaviour
{
    [SerializeField] float speed = 5;
    [SerializeField] float limitRadius = 20;
    [SerializeField] float secondsToChangeDirection = 50f;

    CharacterController controller;
    Vector3 origin;

    void Awake()
    {
        controller = GetComponent<CharacterController>();

        // Set random rotation
        float heading = Random.Range(0, 360);
        transform.eulerAngles = new Vector3(0, heading, heading);
        origin = transform.position;

        StartCoroutine("NewHeading");
    }

    void Update()
    {
        var forward = transform.TransformDirection(Vector3.forward);
        controller.Move(forward * speed);
    }

    IEnumerator NewHeading()
    {
        while (true)
        {
            NewHeadingRoutine();
            yield return new WaitForSeconds(secondsToChangeDirection);
        }
    }

    void NewHeadingRoutine()
    {
        var distanceToOrigin = Vector3.Distance(origin, transform.position);
        if (distanceToOrigin > limitRadius)
        {
            Vector3 toOrigin = origin - transform.position;
            float angleToPosition = transform.eulerAngles.y + Vector3.Angle(transform.forward, toOrigin);
            float heading = Random.Range(angleToPosition - 20f, angleToPosition + 20f);
            transform.eulerAngles = new Vector3(0, heading, heading);
        }
    }
}
