# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - 2020-12-16

### Added

- Set-up HDRP project
- Add markdown files for license and changelog
- Get needed assets from our [tests repository](https://github.com/IMAC-projects/pre-pro-game-tests)

## [Unreleased] - 2020-12-31

### Added

- Add outline shader with HDRP custom passes 
- https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.1/manual/Custom-Pass.html
- https://github.com/alelievr/HDRP-Custom-Passes

## [Unreleased] - 2021-01-07

### Added

- Add spiral effect for the rewind with custom pass. It is active at every moment right now, we need to activate it when the player rewind.

## [Unreleased] - 2021-01-07

### Changed

- HDRP lights are no longer burning the scene
- Toggle rewind spiral VFX only during rewind

### Added

- Disabled rendering pass while spiral VFX is not visible

## [Unreleased] - 2021-01-10

### Removed

- Clean directories by removing useless files

### Added 

- Water unlit shadergraph
